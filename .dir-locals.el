((org-mode . ((eval . (progn
                        (setq-local org-export-smart-quotes-alist
                                    (cons
                                     (cons "en-gb" (cdr (assoc "en" org-export-smart-quotes-alist)))
                                     org-export-smart-quotes-alist))))
              (org-latex-compiler . "lualatex")
              (org-latex-listings . minted)
              (org-latex-pdf-process. ("latexmk -output-directory=%o %f"))
              (org-latex-prefer-user-labels t)
              (org-latex-packages-alist . (("" "booktabs"))))))
